//
//  ViewController.swift
//  LanguageApp
//
//  Created by Đỗ Hưng on 17/11/2021.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var languageLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        languageLabel.text = "ENGLISH".localized
    }
    @IBAction func updateLanguage(_ sender: UISwitch) {
        Language.value = sender.isOn ? .en : .vi
        languageLabel.text = "ENGLISH".localized
    }
}

