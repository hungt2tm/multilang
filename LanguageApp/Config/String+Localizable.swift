//
//  String+Localizable.swift
//  LanguageApp
//
//  Created by Đỗ Hưng on 17/11/2021.
//

import Foundation

extension String {
    var localize: String {
        return Bundle.localizedBundle.localizedString(forKey: self, value: nil, table: nil)
    }
}

extension Bundle {
    static var localizedBundle: Bundle {
        let language = Global.shared.language
        guard let path = Bundle.main.path(forResource: language.rawValue, ofType: "lproj"),
              let bundle = Bundle(path: path) else {
                  return Bundle.main
              }
        return bundle
    }
}

enum Language: String {
    case en = "en"
    case vi = "vi"
    
    static var value: Language {
        get { return Global.shared.language }
        set {
            guard value != newValue else { return }
            UserDefaults.standard.set(newValue.rawValue, forKey: kLanguage)
            Global.shared.language = newValue
            NotificationCenter.default.post(name: .languageNotifName, object: newValue)
        }
    }
}
