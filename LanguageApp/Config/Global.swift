//
//  Global.swift
//  LanguageApp
//
//  Created by Đỗ Hưng on 17/11/2021.
//

import Foundation

let kLanguage = "kLanguage"
struct Global {
    static var shared = Global()
    var language: Language = .en
}

extension Notification.Name {
    static let languageNotifName = Notification.Name("LanguageNotifName")
}
